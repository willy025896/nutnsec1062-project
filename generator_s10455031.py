import sys
def generate():
	filename = sys.argv[1]
	nf_name = "clone_s10455031.py"
	report = "generator_report.json"
	new_file = open(nf_name,"w")
	rfile = open(report,"w")
	for i in range(1,6):
		if i == 1:
			rfile.write("{\n")
			rfile.write('"source file":source_s10455031.py,\n')
			rfile.write('"clone file":clone_s10455031.py,\n')
			rfile.write('"clone 1":{\n\t')
			rfile.write('"clone type":"1",\n\t"code fragment":{')
			ofile = open(filename,"r")
			a=0
			for line in iter(ofile):
				new_file.write(line)
				a=a+1
			rfile.write('"start line":')
			rfile.write(str(a+1))
			rfile.write(',"end line":')
			rfile.write(str(a+1))
			rfile.write('},\n"code clone":{"start line":')
			rfile.write(str(0))
			rfile.write(', "end line":')
			rfile.write(str(a+6))
			rfile.write("}\n}\n")
			ofile.close()
			new_file.write("##context")
			for l in range(4):
				new_file.write('\n')
			new_file.write("##========================================\n")
		if i == 2:
			rfile.write('"clone 2":{\n\t')
			rfile.write('"clone type":"3",\n\t"code fragment":{')
			ofile = open(filename,"r")
			for line in iter(ofile):
				new_file.write(line)
			rfile.write('"start line":')
			rfile.write(str((2*a)+8))
			rfile.write(',"end line":')
			rfile.write(str((2*a)+9))
			rfile.write('},\n"code clone":{"start line":')
			rfile.write(str(a+7))
			rfile.write(', "end line":')
			rfile.write(str((2*a)+14))
			rfile.write("}\n}\n")
			ofile.close()
			new_file.write("for i in range(10):\n\tXDDDDDD=0")
			for l in range(4):
				new_file.write('\n')
			new_file.write("##========================================\n")
		if i == 3:
			rfile.write('"clone 3":{\n\t')
			rfile.write('"clone type":"3",\n\t"code fragment":{')
			ofile = open(filename,"r")
			for line in iter(ofile):
				new_file.write(line)
			rfile.write('"start line":')
			rfile.write(str((3*a)+16))
			rfile.write(',"end line":')
			rfile.write(str((3*a)+16))
			rfile.write('},\n"code clone":{"start line":')
			rfile.write(str((2*a)+15))
			rfile.write(', "end line":')
			rfile.write(str((3*a)+21))
			rfile.write("}\n}\n")
			ofile.close()
			new_file.write("fake = list()\n")
			for l in range(4):
				new_file.write('\n')
			new_file.write("##========================================\n")
		if i == 4:
			rfile.write('"clone 4":{\n\t')
			rfile.write('"clone type":"1",\n\t"code fragment":{')
			ofile = open(filename,"r")
			for line in iter(ofile):
				new_file.write(line)
			new_file.write('\n')
			rfile.write('"start line":')
			rfile.write(str((4*a)+23))
			rfile.write(',"end line":')
			rfile.write(str((4*a)+23))
			rfile.write('},\n"code clone":{"start line":')
			rfile.write(str((3*a)+22))
			rfile.write(', "end line":')
			rfile.write(str((4*a)+27))
			rfile.write("}\n}\n")
			ofile.close()
			for l in range(4):
				new_file.write('\n')
			new_file.write("##========================================\n")
		if i == 5:
			rfile.write('"clone 5":{\n\t')
			rfile.write('"clone type":"3",\n\t"code fragment":{')
			ofile = open(filename,"r")
			for line in iter(ofile):
				new_file.write(line)
			rfile.write('"start line":')
			rfile.write(str((5*a)+29))
			rfile.write(',"end line":')
			rfile.write(str((5*a)+30))
			rfile.write('},\n"code clone":{"start line":')
			rfile.write(str((4*a)+28))
			rfile.write(', "end line":')
			rfile.write(str((5*a)+34))
			rfile.write("}\n}\n")
			ofile.close()
			new_file.write("for i in range(5):\n\tXDDDDDD=0")
			for l in range(4):
				new_file.write('\n')
			new_file.write("##========================================\n")
	new_file.close()
generate()

